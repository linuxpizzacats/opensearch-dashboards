FROM debian:11.0-slim
ARG OPENSEARCH_DASHBOARDS_DOWNLOAD_URL="https://artifacts.opensearch.org/releases/bundle/opensearch-dashboards/1.0.1/opensearch-dashboards-1.0.1-linux-x64.tar.gz"

#install some tools
RUN apt update && apt install -y curl wget procps vim

#create user for run opensearch
RUN useradd -u 5000 opensearch

#download opensearch and untar it
RUN wget $OPENSEARCH_DASHBOARDS_DOWNLOAD_URL -O - | tar xz && mv opensearch-* opensearch-dashboards
WORKDIR /opensearch-dashboards
RUN chmod 755 -R /opensearch-dashboards && chown -R opensearch:opensearch /opensearch-dashboards

USER opensearch:opensearch

VOLUME ["/opensearch-dashboards/data", "/opensearch-dashboards/config"]

#opensearch listen port
EXPOSE 5601 

ENTRYPOINT ["/bin/bash","-c","/opensearch-dashboards/bin/opensearch-dashboards"]

